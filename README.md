# DataBrockerService

PHP package for interaction with ODI CRM API

## TAGS
 The tags respect the [semantic versionning](https://semver.org/) 
 - Major, Minor, Patch (Minor and Patch are 100% compatibles when Major needs code updates)
/!\ 0.7 to 0.8 needs a small code update.

# TESTS
- The created Individuals will all have the same end of email '@databrocker.fr'. To help finding them in 
  the CRM.
## Launch tests
``` vendor/bin/phpunit tests/IndividualTest.php --colors always --coverage-html tests/results```

# RELEASES
0.8.1 fix error on getIndividualWithoutDuplication when individuals had no subscriptions.

0.8 fix getIndividualWithoutDuplication return. It now **always retun an Individual** or false. Before it could return the Individual Id in a string.
        Code update is needed

2.0 Switch to the new (2023) API version of ODI.
- Function Individual->setEndDateToSubscription() has been removed.
- Function Individual->getActiveIndividualId() has been removed.
