<?php

namespace PO;

use Brevo\Client\Api\TransactionalEmailsApi;
use Brevo\Client\ApiException;
use Brevo\Client\Model\GetSmtpTemplateOverview;
use Brevo\Client\Model\GetTransacEmailContent;
use Brevo\Client\Model\GetTransacEmailsListTransactionalEmails;
use Brevo\Client\Model\SendSmtpEmail;
use Brevo;
use Exception;
use GuzzleHttp\Client;


Class EmailSender
{

    private static EmailSender $_instance;

    public static function getInstance(): EmailSender
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new EmailSender();
        }
        return self::$_instance;
    }

    /**
     * @return TransactionalEmailsApi
     */
    private static function getApiInstance(): TransactionalEmailsApi
    {
        // Configure API key authorization: api-key
        $config = Brevo\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');

        return new TransactionalEmailsApi(
            new Client(),
            $config
        );
    }

    /**
     * @param array $emailsTo Example ['toto@po.fr', 'titi@to.com']
     * @param int $templateId
     * @param array $params
     * @param array $emailsBcc Example ['toto@po.fr', 'titi@to.com']
     * @return bool|Exception
     */
    public static function sendTransactionalTemplate(
        array $emailsTo,
        int   $templateId,
        array $params = [],
        array $emailsBcc = array()
    ): bool|Exception
    {
        $apiInstance = self::getApiInstance();
        list($emailToFormatted, $emailBccFormatted) = self::formatEmails($emailsTo, $emailsBcc);

        if(!empty($emailBccFormatted)) {
            if(!empty($params)) {
                $sendSmtpEmail = new SendSmtpEmail(
                    [
                        'to' => $emailToFormatted,
                        'bcc' => $emailBccFormatted,
                        'templateId' => $templateId,
                        'params' => $params
                    ]
                );
            } else {
                $sendSmtpEmail = new SendSmtpEmail(
                    [
                        'to' => $emailToFormatted,
                        'bcc' => $emailBccFormatted,
                        'templateId' => $templateId,
                    ]
                );
            }

        } else {
            if (!empty($params)) {
                $sendSmtpEmail = new SendSmtpEmail(
                    [
                        'to' => $emailToFormatted,
                        'templateId' => $templateId,
                        'params' => $params
                    ]
                );
            } else {
                $sendSmtpEmail = new SendSmtpEmail(
                    [
                        'to' => $emailToFormatted,
                        'templateId' => $templateId,
                    ]
                );
            }
        }


        try {
            $apiInstance->sendTransacEmail($sendSmtpEmail);
            return true;
        } catch (Exception $e) {
            echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
            return $e;
        }
    }


    /**
     * @param array $emailsTo
     * @param string $htmlContent
     * @param string $subject
     * @param string $sender
     * @param array $emailsBcc
     * @return bool|Exception
     * @description  <!DOCTYPE html><html><body><h1>Exemple</h1><p>toto</p></body></html>
     */
    public static function sendAdminEmails(
        array $emailsTo,
        string $htmlContent,
        string $subject,
        string $sender = 'webmaster@portesouvertes.fr',
        array $emailsBcc = array ()
    ): bool|Exception
    {

        $apiInstance = self::getApiInstance();
        list($emailToFormatted, $emailBccFormatted, $emailSenderFormatted) = self::formatEmails($emailsTo,
            $emailsBcc, $sender);

        if(!empty($emailBccFormatted)) {
            $sendEmail = new SendSmtpEmail(
                [
                    'to' => $emailToFormatted,
                    'bcc' => $emailBccFormatted,
                    'htmlContent' => $htmlContent,
                    'subject' => $subject,
                    'sender' => $emailSenderFormatted
                ]
            );
        } else {
            $sendEmail = new SendSmtpEmail(
                [
                    'to' => $emailToFormatted,
                    'htmlContent' => $htmlContent,
                    'subject' => $subject,
                    'sender' => $emailSenderFormatted,
                ]
            );
        };

        try {
            $apiInstance->sendTransacEmail($sendEmail);
            return true;
        } catch (Exception $e) {
            echo 'Exception error on email sending with SendInBlue: ', $e->getMessage(), PHP_EOL;
            return $e;
        }
    }

    /**
     * @param array $emailsTo
     * @param array $emailsBcc
     * @param string $emailOfTheSender
     * @return array[]
     */
    private static function formatEmails(array $emailsTo, array $emailsBcc, string $emailOfTheSender = ''):
    array
    {
        $emailToFormatted = array();
        foreach ($emailsTo as $email) {
            $emailToFormatted[] = array('email' => $email);
        }

        $emailBccFormatted = array();
        if(!empty($emailsBcc)) {
            foreach ($emailsBcc as $email) {
                $emailBccFormatted[] = array('email' => $email);
            }
        }

        if ($emailOfTheSender) {
            return array($emailToFormatted, $emailBccFormatted, ['email' => $emailOfTheSender]);
        }
        return array($emailToFormatted, $emailBccFormatted);
    }

    /**
     * @param string|null $email
     * @param int|null $templateId
     * @param string|null $startDate exemple: now()->subWeek()->format('Y-m-d')
     * @param string|null $endDate exemple: now()->format('Y-m-d')
     * @param string $sort
     * @param int $limit
     * @return array|null
     * @throws ApiException
     * @description https://developers.brevo.com/reference/gettransacemailslist
     */
    public function getTransactionalEmails(string $email = null, int $templateId = null, string $startDate =
        null, string $endDate = null, string $sort = 'desc', int $limit = 1 ): array|null
    {
       $apiInstance = self::getApiInstance();

        return $apiInstance->getTransacEmailsList($email, $templateId, null, $startDate, $endDate, $sort,
            $limit)->getTransactionalEmails();
    }


    /**
     * @param string $templateUuid
     * @return GetTransacEmailContent
     * @throws ApiException
     */
    public function getTemplateInformation(string $templateUuid): Brevo\Client\Model\GetTransacEmailContent
    {
        $apiInstance = self::getApiInstance();
        return $apiInstance->getTransacEmailContent($templateUuid);
    }

}

