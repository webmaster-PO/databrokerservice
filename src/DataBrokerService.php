<?php

namespace PO;

use Exception;
use stdClass;

class DataBrokerService
{
    use Guids;

    private string $apiKey;
    private string $endPoint;

    /**
     * CRMDBS constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->apiKey = $params['apiKey'];
        $this->endPoint = $params['endPoint'];
    }

    /**
     * Curl call
     * @param string $url
     * @param bool $isPostRequest
     * @param array|string|null $callParameters
     * @return bool|array|string|stdClass
     */

    protected function callDBS(string $url, bool $isPostRequest = false, array|string|null $callParameters =
    []): mixed
    {

        $curl = curl_init();

        $curlOptions = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->endPoint . $url,
            CURLOPT_HTTPAUTH => CURLAUTH_NTLM,
            CURLOPT_HEADER => 1,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ];

        // Add parameters if it's a post call. null is a value as it can be used in
        // Subscriptions/ChangeProduct
        if ($isPostRequest) {
            $curlOptions[CURLOPT_POST] = $callParameters ? 1 : ($callParameters === null ? 1 : 0);
            $curlOptions[CURLOPT_POSTFIELDS] = json_encode($callParameters);
            $curlOptions[CURLOPT_HTTPHEADER] = ['APIKey: ' . $this->apiKey, 'Content-Type:application/json'];
        } else {
            $curlOptions[CURLOPT_HTTPHEADER] = ['APIKey: ' . $this->apiKey];
        }

        curl_setopt_array($curl, $curlOptions);

        // Send the request & return the response*
        $attempt = 0;
        while ($attempt < 2) {
            try {
                // wait if seconde call attempt.
                $attempt && sleep(20);
                $attempt ++;

                $resp = curl_exec($curl);

                $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $firstNumberStatusCode = substr($statusCode, 0, 1);

                if($firstNumberStatusCode == 4 || $firstNumberStatusCode == 5) {

                    $error_message = curl_error($curl)?: 'error message not specified';
                    $error_code = curl_errno($curl);

                    // Lever une exception manuellement pour que le catch soit exécuté
                    throw new Exception($error_message, $error_code);
                }
                break;
            } catch (Exception $e) {
                if($attempt > 1) {
                    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                    $emailSender = new EmailSender();
                    $emailContent = '<html lang="en-EN"><body><h1>An error append in callDBS in the DBS package. Status code : ' .
                        $statusCode . '</h1><br><p>Url of the call : ' . $this->endPoint . '<br><br>
                Data sent : ' . json_encode($callParameters) . '<br><br>URL: ' .
                        $url . '</p><br>Tentative num: ' . $attempt . ' <br><p>
                Resultat: '. $resp .'<br></p><p>
                
                    Erreur: ' . $e->getMessage() . '</p></body></html>';

                    $emailSender->sendAdminEmails(
                        ['michaelj@portesouvertes.fr', 'webmaster@portesouvertes.fr'],
                        $emailContent,
                        'Error DBS package, a CURL call failed',
                        'webmaster@portesouvertes.fr',
                        ['pierreb@portesouvertes.fr']
                    );
                }
            }
        }
        curl_close($curl);

        list($header, $body) = explode("\r\n\r\n", $resp, 2);

        if (empty(json_decode($body))) {
            return false;
        }
        return json_decode($body);
    }

    public function marketingList(): MarketingList
    {
        return MarketingList::getInstance(['apiKey' => $this->apiKey, 'endPoint' => $this->endPoint]);
    }

    public function activity(): Activity
    {
        return Activity::getInstance(['apiKey' => $this->apiKey, 'endPoint' => $this->endPoint]);
    }

    public function donation(): Donation
    {
        return Donation::getInstance(['apiKey' => $this->apiKey, 'endPoint' => $this->endPoint]);
    }

    public function group(): Group
    {
        return Group::getInstance(['apiKey' => $this->apiKey, 'endPoint' => $this->endPoint]);
    }

    public function individual(): Individual
    {
        return Individual::getInstance(['apiKey' => $this->apiKey, 'endPoint' => $this->endPoint]);
    }

    public function subscription(): Subscription
    {
        return Subscription::getInstance(['apiKey' => $this->apiKey, 'endPoint' => $this->endPoint]);
    }

    public function emailSender(): EmailSender
    {
        return EmailSender::getInstance();
    }

    public function contactBrevo(): BrevoContact
    {
        return BrevoContact::getInstance();
    }

    /**
     * @return array
     * @description Do not use this method for subscriptions as this list is not up-to-date, prefer to use
     * getSubscriptionGuidFromName.
     */
    public function guidsCrm(): array
    {
        return $this->guids();
    }

    /**
     * @return string[]
     *  Return list of countries array [key => value, ...]. Update 02/2021 from CRM
     */
    public static function getCountries(): array
    {
        $countries = [
            "France",
            "AFRIQUE DU SUD",
            "ALBANIE",
            "ALGERIE",
            "ALLEMAGNE",
            "ANGOLA",
            "ARABIE SAOUDITE",
            "ARGENTINE",
            "ARMENIE",
            "AUSTRALIE",
            "AUTRICHE",
            "BAHREIN",
            "BANGLADESH",
            "BELGIQUE",
            "BENIN",
            "BELARUS",
            "BOLIVIE",
            "BOSNIE HERZEGOVINE",
            "BRESIL",
            "BULGARIE",
            "BURKINA FASO",
            "BURUNDI",
            "CAMBODGE",
            "CAMEROUN",
            "CANADA",
            "CHILI",
            "CHINE",
            "CHYPRE",
            "COLOMBIE",
            "COMORES",
            "CONGO",
            "COREE DU NORD",
            "COSTA RICA",
            "COTE D IVOIRE",
            "CROATIE",
            "DANEMARK",
            "DOMINIQUE",
            "EGYPTE",
            "EMIRATS ARABES UNIS",
            "EQUATEUR",
            "ESPAGNE",
            "ESWATINI",
            "ETATS UNIS",
            "ETHIOPIE",
            "FINLANDE",
            "GABON",
            "GHANA",
            "GRANDE BRETAGNE",
            "GRECE",
            "GUATEMALA",
            "GUINEE",
            "GUINEE EQUATORIALE",
            "HAITI",
            "HONG KONG",
            "HONGRIE",
            "INDE",
            "INDONESIE",
            "IRAN",
            "IRLANDE",
            "IRLANDE DU NORD",
            "ISLANDE",
            "ISRAEL",
            "ITALIE",
            "JAPON",
            "JERSEY",
            "JORDANIE",
            "KAZAKHSTAN",
            "KENYA",
            "KIRGHIZISTAN",
            "LIBAN",
            "LIBERIA",
            "LITUANIE",
            "LUXEMBOURG",
            "LIBYE",
            "MACEDOINE DU NORD",
            "MADAGASCAR",
            "MALAISIE",
            "MALAWI",
            "MALDIVES",
            "MALI",
            "MALTE",
            "MAROC",
            "MAURICE",
            "MAURITANIE",
            "MEXIQUE",
            "MOLDAVIE",
            "MONACO",
            "MOZAMBIQUE",
            "NIGER",
            "NIGERIA",
            "NORVEGE",
            "NOUVELLE ZELANDE",
            "OUGANDA",
            "OUZBEKISTAN",
            "PAKISTAN",
            "PANAMA",
            "PARAGUAY",
            "PAYS BAS",
            "PEROU",
            "PHILIPPINES",
            "POLOGNE",
            "PORTUGAL",
            "QATAR",
            "REP DEM DU CONGO",
            "REPUBLIQUE CENTRAFRICAINE",
            "REPUBLIQUE DE COREE",
            "REPUBLIQUE DOMINICAINE",
            "ROUMANIE",
            "RUSSIE",
            "RWANDA",
            "SAINTE LUCIE ILES DU VENT",
            "SALVADOR",
            "SENEGAL",
            "SERBIE",
            "SEYCHELLES",
            "SINGAPOUR",
            "SLOVAQUIE",
            "SLOVENIE",
            "SOUDAN",
            "SUEDE",
            "SUISSE",
            "SYRIE",
            "TADJIKISTAN",
            "TAIWAN",
            "TANZANIE",
            "TCHAD",
            "TCHEQUIE",
            "THAILANDE",
            "TOGO",
            "TUNISIE",
            "TURQUIE",
            "UKRAINE",
            "URUGUAY",
            "VANUATU",
            "VATICAN",
            "VENEZUELA",
            "VIET NAM",
            "ZAMBIE",
            "ZIMBABWE"
        ];
        $formatedCountries = [];
        foreach ($countries as $country) {
            $formatedCountries[$country] = $country;
        }

        return $formatedCountries;
    }

    /**
     * @return array|false
     * @throws Exception
     */
    public function getAllMediaCodes(): bool|array
    {
        $url = "MediaCode/GetPaged";
        return $this->callDBS($url,false, ['page' => 1, 'pageSize' => 1200]);
    }

    /**
     * @param string $mediaCodeName
     * @return string
     * @throws Exception
     * @description If the media code is not found Guid of IN will be returned.
     */
    public function getMediaCodeGuidFromName(string $mediaCodeName): string {

        $allMediaCodes = $this->getAllMediaCodes();
        $defaultMediaCode = '';

        foreach ($allMediaCodes as $mediaCode) {
            if($mediaCode->MediaCode == $mediaCodeName) {
                return $mediaCode->Id;
            }
            if($mediaCode->MediaCode == 'IN') {
                $defaultMediaCode = $mediaCode->Id;
            }
        }
        return $defaultMediaCode;

    }

}
