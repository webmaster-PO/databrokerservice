<?php

namespace PO;

use Brevo\Client\Api\ContactsApi;
use Brevo\Client\ApiException;
use Brevo;
use Exception;
use GuzzleHttp\Client;


Class BrevoContact
{

    private static BrevoContact $_instance;

    public static function getInstance(): BrevoContact
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new BrevoContact();
        }
        return self::$_instance;
    }

    /**
     * @return ContactsApi
     */
    private static function getApiInstance(): ContactsApi
    {
        // Configure API key authorization: api-key
        $config = Brevo\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');

        return new ContactsApi(
            new Client(),
            $config
        );
    }


    /**
     * @param string $email
     * @return Brevo\Client\Model\GetExtendedContactDetails
     * @throws ApiException
     */
    public function getContactDetails(string $email): Brevo\Client\Model\GetExtendedContactDetails
    {
        $apiInstance = self::getApiInstance();
        return $apiInstance->getContactInfo($email);
    }

}

