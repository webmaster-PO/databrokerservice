<?php

namespace PO;

use Exception;
use stdClass;

class Group extends DataBrokerService
{

    private static Group $_instance;

    public static function getInstance($params): Group
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new Group($params);
        }
        return self::$_instance;
    }

    /**
     * @throws Exception
     */
    public function findGroupByEmailAddress(string $groupEmail)
    {

        $url = "Group/GetAllByEmailAddress/{$groupEmail}/?email={$groupEmail}";

        return $this->callDBS($url, false, ['email' => $groupEmail]);
    }

    /**
     * @throws Exception
     */
    public function findGroupsByZipCode(string $zipCode)
    {

        $url = 'GroupSearch/Get?Address1ZipPostalCode='.$zipCode;

        return $this->callDBS($url);
    }

    /**
     * @throws Exception
     */
    public function connectGroupToIndividual(string $groupID, string $individualID)
    {
        $connections = $this->individual()->getConnectionsOfTheIndividual($individualID);
        $isConnected = false;

        if (!empty($connections)) {
            foreach ($connections as $oneConnection) {
                if ($oneConnection->ToId == $groupID) {
                    return true;
                }
            }
        }

        $url = 'GroupToIndividualConnection/Post';
        $connection = [
            'ConnectedFrom' => $groupID,
            'ConnectedTo' => $individualID,
            'RoleFrom' => 'Church',
            'RoleTo' => 'Church member',
            'Starting' => date('Y-m-d'),
        ];

        return $this->callDBS($url, true, $connection);

    }

    /**
     * @throws Exception
     */
    public function getChurchById(string $groupID)
    {
        $url = "Group/Get/{$groupID}";

        return $this->callDBS($url);
    }

    /**
     * @param array $fields
     * @return bool|array|stdClass
     */
    public function createOrUpdateGroup(array $fields): bool|array|stdClass
    {
        $url = 'Group/Post';

        return $this->callDBS($url, true, $fields);
    }

    /**
     * @param string $groupId
     * @return false|array
     */
    public function getGroupAllSubscriptionsById(string $groupId)
    {
        $url = "Group/SubscriptionsAll/{$groupId}";

        return $this->callDBS($url);
    }

    /**
     * @param string $groupId
     * @param array $subscriptions
     * @return false|array
     */
    public function addGroupSubscriptions(string $groupId, array $subscriptions)
    {
        $url = "Group/SubscriptionsAdd/{$groupId}";

        return $this->callDBS($url, true, $subscriptions);
    }

    /**
     * @param string $groupId
     * @param array $subscriptions
     * @return false|array
     */
    public function setEndDateToGroupSubscriptions(string $groupId, array $subscriptions)
    {
        $url = "Group/SubscriptionsEnd/{$groupId}";

        return $this->callDBS($url, true, $subscriptions);
    }
}
