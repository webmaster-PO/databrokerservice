<?php


namespace PO;

/**
 * Trait Guids
 * @package PO
 */
trait Guids
{

    public function guids(): array
    {

            // mediacodes last update: 04/2023 with type startcodes
        // Some of those Ids are used in the DBS, and in some applications. Avoid as much as possible to
        // use it, as they will be removed when all the applications won't use it anymore. Prefer to use
        // the function Subscription->getSubscriptionGuidFromName() instead for exemple.
        return [

            'mediaCodes' => [
                'IN' => "7c1034cf-6fbe-e511-9414-00155d0c180d",
                'SHOCKW' => "c21134cf-6fbe-e511-9414-00155d0c180d",
                'IN-ARPM' => "29df06cb-31b1-ed11-83ff-0022489ae0f1",
                'IN-ARPM-DEV' => "048f410b-35b1-ed11-83ff-0022489ede2f",
                'PRJ' => '234332fc-89ec-e611-9402-00155d0c1cd0',
                'PREJ' => 'eccca7ef-5af7-e611-9402-00155d0c1cd0',
                'PRO' => '72f2d063-e60e-e711-9403-00155d0c1cd0',
                'M5TR' => '94e8ec0f-2515-e711-9403-00155d0c1cd0',
                'M5POJ' => '78e4512c-2515-e711-9403-00155d0c1cd0',
                'LIB-CONT' => 'a34ff433-18df-e711-940e-00155d0c2444',
                'MA' => 'd81034cf-6fbe-e511-9414-00155d0c180d',
                'OR' => 'db1034cf-6fbe-e511-9414-00155d0c180d',
                'EJ' => 'c28a3bc9-6fbe-e511-9414-00155d0c180d',
                '30JRS' => 'f21034cf-6fbe-e511-9414-00155d0c180d',
                'CC' => '771134cf-6fbe-e511-9414-00155d0c180d',
                'CF' => '821134cf-6fbe-e511-9414-00155d0c180d',
                'DEP' => 'a31134cf-6fbe-e511-9414-00155d0c180d',
                'AC' => 'b11134cf-6fbe-e511-9414-00155d0c180d',
                'EV' => 'b41134cf-6fbe-e511-9414-00155d0c180d',
                'EXP' => 'b51134cf-6fbe-e511-9414-00155d0c180d',
                'FL' => 'b61134cf-6fbe-e511-9414-00155d0c180d',
                'M5' => 'b81134cf-6fbe-e511-9414-00155d0c180d',
                'AE' => 'bc1134cf-6fbe-e511-9414-00155d0c180d',
                'PR' => 'be1134cf-6fbe-e511-9414-00155d0c180d',
                'PRESSE' => 'bf1134cf-6fbe-e511-9414-00155d0c180d',
                'PT' => 'c01134cf-6fbe-e511-9414-00155d0c180d',
                'QD' => 'c11134cf-6fbe-e511-9414-00155d0c180d',
                'SPOR' => 'c31134cf-6fbe-e511-9414-00155d0c180d',
                'TE' => 'c41134cf-6fbe-e511-9414-00155d0c180d',
                'TF' => 'c51134cf-6fbe-e511-9414-00155d0c180d',
                'TH' => 'c61134cf-6fbe-e511-9414-00155d0c180d',
                'AN' => 'c71134cf-6fbe-e511-9414-00155d0c180d',
                'VO' => 'c81134cf-6fbe-e511-9414-00155d0c180d',
                'WE' => 'c91134cf-6fbe-e511-9414-00155d0c180d',
                'ECVL' => 'cd1134cf-6fbe-e511-9414-00155d0c180d',
                'PL' => 'ce1134cf-6fbe-e511-9414-00155d0c180d',
                'MD' => 'cf1134cf-6fbe-e511-9414-00155d0c180d',
                'CP' => 'd51134cf-6fbe-e511-9414-00155d0c180d',
                'DO' => 'd71134cf-6fbe-e511-9414-00155d0c180d',
                'M5CF' => 'f3690fc8-6d3c-e711-9406-00155d0c1817',
                'AGIR' => 'b733cd05-700f-ea11-9428-00155d0c1856',
                'PL-CP' => '7b3c8aee-8482-e711-940a-00155d0c186a',
                'PL-IND' => '8b275273-8582-e711-940a-00155d0c186a',
                'PL-EV' => '780073ad-8582-e711-940a-00155d0c186a',
                'BEN' => '6c43160a-ba17-e811-9411-00155d0c18a3',
                'IN NL' => 'db3df676-9627-e811-9411-00155d0c18a3',
                'BUR' => 'cc97ef69-c85e-ea11-942e-00155d0c1923',
                'RS' => 'be0564cd-5a6d-e711-9408-00155d0c1929',
                'IN CC' => '871034cf-6fbe-e511-9414-00155d0c180d',
                'IN CF' => '921034cf-6fbe-e511-9414-00155d0c180d',
                'AMB' => 'f23459b6-b7b1-e811-941a-00155d0c19b2',
                'CAN' => 'ec1d2d18-5ecd-e911-9424-00155d0c19fa',
                'PLNL' => '28ead63e-f27c-eb11-a812-0022489b074f',
                'IND21' => 'ecb9de4f-facc-eb11-bacc-000d3ab02693',
                'SPORD' => '77af512e-70cf-eb11-bacc-000d3ab02314',
                'ARPM' => '8eb290dc-ed88-ec11-93b0-0022489de1df',
                'INLI' => '46301b68-9898-ed11-aad1-0022489aed7e',
                'IN ADF' => 'ce56d6ad-d9c7-ed11-b597-0022489ae0f1',
                'AFSUBS23' => 'b60c3d02-a42f-ee11-bdf4-0022489ae1c4'
            ],
            'civilities' => [
                'MS' => 'c6a47d99-6fbe-e511-9414-00155d0c180d',
                'MM' => 'c4a47d99-6fbe-e511-9414-00155d0c180d',
                'ML' => 'c3a47d99-6fbe-e511-9414-00155d0c180d',
                'M.' => 'c6a47d99-6fbe-e511-9414-00155d0c180d',
                'Mme' => 'c4a47d99-6fbe-e511-9414-00155d0c180d',
                'Mlle' => 'c3a47d99-6fbe-e511-9414-00155d0c180d',
                'ME' => 'c2a47d99-6fbe-e511-9414-00155d0c180d'
            ],
            'subscriptions' => [
                'CONT' => 'e0131bd6-6fbe-e511-9414-00155d0c180d',
                'EINFO' => 'f0131bd6-6fbe-e511-9414-00155d0c180d',
                'EADF' => 'e7131bd6-6fbe-e511-9414-00155d0c180d',
                'ENL' => '664bddaa-9627-e811-9411-00155d0c18a3',
                'NLCDN' => 'e4131bd6-6fbe-e511-9414-00155d0c180d',
                'FL' => 'f4131bd6-6fbe-e511-9414-00155d0c180d',
                'PLNL' => 'daaf95a5-bf06-ea11-9427-00155d0c1902',
                'TAX' => 'da2bfd9f-2f44-e611-941d-00155d0c18a0', //used
                'TYL' => 'f6131bd6-6fbe-e511-9414-00155d0c180d',
                'ETYL' => 'ee131bd6-6fbe-e511-9414-00155d0c180d',
                'ERF' => 'cbc5c7eb-618c-e611-9422-00155d0c24ab',
                'RF' => 'f5131bd6-6fbe-e511-9414-00155d0c180d',
                'ARPM' => '8928bf2f-ed88-ec11-93b0-0022489de1df',
                'ARPM_DEV' => '24c35adc-9c8d-ec11-b400-0022489cecc8',
                'EM-AFGHA22' => '6a8d9a70-c5f6-ec11-bb3d-0022489f2762',
                'EM-AFGHA22_DEV' => '9a388ee3-75fb-ec11-82e5-000d3ade72a1',
                'EM-AFSUBS23' => '48844841-542d-ee11-bdf4-0022489aed7e'
            ],
            'countries' => [
                'France','ALBANIE', 'ALGERIE', 'ALLEMAGNE', 'ANGOLA', 'ARABIE SAOUDITE', 'ARGENTINE', 'ARMENIE', 'AUSTRALIE', 'AUTRICHE', 'BAHREIN', 'BELGIQUE', 'BENIN', 'BIELORUSSIE', 'BOSNIE HERZEGOVINE', 'BRESIL', 'BULGARIE', 'BURKINA FASO', 'BURUNDI', 'CAMBODGE', 'CAMEROUN', 'CANADA', 'CHILI', 'CHINE', 'CHYPRE', 'COLOMBIE', 'COMORES', 'CONGO', 'COREE DU SUD', 'COSTA RICA', 'COTE D\'IVOIRE', 'DANEMARK', 'EGYPTE', 'EMIRATS ARABES UNIS', 'EQUATEUR', 'ESPAGNE', 'ETATS-UNIS', 'ETHIOPIE', 'FINLANDE', 'GABON', 'GHANA', 'GRANDE BRETAGNE', 'GRECE', 'GUATEMALA', 'GUINEE', 'GUINEE EQUATORIALE', 'HAITI', 'HONG KONG', 'HONGRIE', 'ILE MAURICE', 'INDE', 'INDONESIE', 'IRAN', 'IRLANDE', 'IRLANDE DU NORD', 'ISLANDE', 'ISRAEL', 'ITALIE', 'JAPON', 'JERSEY', 'KAZAKHSTAN', 'KENYA', 'LIBAN', 'LIBERIA', 'LITUANIE', 'LUXEMBOURG', 'LYBIE', 'MADAGASCAR', 'MALAISIE', 'MALAWI', 'MALI', 'MAROC', 'MAURITANIE', 'MEXICO', 'MONACO', 'NIGER', 'NIGERIA', 'NORVEGE', 'NOUVELLE ZELANDE', 'OUGANDA', 'PANAMA', 'PARAGUAY', 'PAYS BAS', 'PEROU', 'PHILIPPINES', 'POLOGNE', 'PORTUGAL', 'QATAR', 'REP. DéM. DU CONGO', 'REPUBLIQUE CENTRAFRICAINE', 'REPUBLIQUE D\'AFRIQUE DU SUD', 'REPUBLIQUE DOMINICAINE', 'REPUBLIQUE TCHEQUE', 'ROUMANIE', 'RUSSIE', 'RWANDA', 'SAINTE LUCIE', 'SALVADOR', 'SENEGAL', 'SERBIE', 'SEYCHELLES', 'SINGAPOUR', 'SLOVAQUIE', 'SLOVENIE', 'SOUDAN', 'SUEDE', 'SUISSE', 'SWAZILAND', 'SYRIE', 'TAÏWAN', 'TANZANIE', 'TCHAD', 'THAILANDE', 'TOGO', 'TUNISIE', 'TURQUIE', 'UKRAINE', 'URUGUAY', 'VANUATU', 'VATICAN', 'VENEZUELA', 'ZAMBIE', 'ZIMBABWE'
            ]
        ];
    }

}