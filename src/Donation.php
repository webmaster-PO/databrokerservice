<?php


namespace PO;

use Exception;

class Donation extends DataBrokerService
{

    private static Donation $_instance;


    public static function getInstance($params): Donation
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new Donation($params);
        }
        return self::$_instance;
    }


    /**
     * @param array $donation
     * @return array|false
     * @throws Exception
     */
    public function addDonationToTheBatch(array $donation)
    {
        $url = 'Batch/AddDonationToLatestActiveBatchFromWeb';

        return $this->callDBS($url, true, $donation);
    }

    /**
     * Get destination id based on baseReference
     * @param string $baseReference
     * @return false|string
     * @throws Exception
     */
    public function getDestinationId(string $baseReference): false|string
    {
        $url = "Destination/Get";

        $destinations = $this->callDBS($url);

        foreach ($destinations as $item) {
            if(isset($item->BaseReference) && $item->BaseReference == $baseReference) {
                return $item->Id;
            }
        }
        return false;
    }

}