<?php

namespace PO;

use Exception;

class Activity extends DataBrokerService
{

    private static Activity $_instance;

    public static function getInstance($params): Activity
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new Activity($params);
        }
        return self::$_instance;
    }

    /**
     * @param string $individualID
     * @param string $subject
     * @return array|false
     * @throws Exception
     */
    public function addLetterActivity(string $individualID, string $subject)
    {
        $url = "IndividualActivities/AddLetter/{$individualID}";
        $activity = [
            'Subject' => $subject,
            'IndividualId' => $individualID,
        ];

        return $this->callDBS($url, true, $activity);
    }

    /**
     * @param string $groupID
     * @param string $subject
     * @return array|false
     * @throws Exception
     */
    public function addLetterActivityToGroup(string $groupID, string $subject)
    {
        $url = "GroupActivities/AddLetter/{$groupID}";
        $activity = [
            'Subject' => $subject,
            'GroupId' => $groupID,
        ];

        return $this->callDBS($url, true, $activity);
    }

}