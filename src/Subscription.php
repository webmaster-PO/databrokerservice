<?php

namespace PO;

use Exception;

class Subscription extends DataBrokerService
{

    private static Subscription $_instance;

    public static function getInstance($params): Subscription
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new Subscription($params);
        }
        return self::$_instance;
    }


    /**
     * @param string $individualGuid
     * @param string $subscriptionGuid
     * @return bool
     * @throws Exception
     */
    public function hasSubscription(string $individualGuid, string $subscriptionGuid): bool
    {
        $subscriptions = $this->getIndividualAllSubscriptionsById($individualGuid);

        if ($subscriptions) {
            foreach ($subscriptions as $subscription) {
                if ($subscription->Subscription->Id == $subscriptionGuid) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    /**
     * @param string $email
     * @return false|array
     * @throws Exception
     */
    public function getIndividualSubscriptions(string $email)
    {
        $individualId = $this->individual()->getIndividualId($email, true);

        if ($individualId) {
            $url = "Individual/Subscriptions/{$individualId}";

            return $this->callDBS($url);
        } else {
            return false;
        }
    }

    /**
     * @param string $individualId
     * @return false|array
     * @throws Exception
     */
    public function getIndividualAllSubscriptionsById(string $individualId)
    {
        $url = "Individual/SubscriptionsAll/{$individualId}";

        return $this->callDBS($url);
    }

    //Le return false permet de savoir si on a eu besoin de faire le remove ou non. (on se place du coté de la function et non du crm).

    /**
     * @throws Exception
     */
    public function removeTylTax(string $individualId): bool|string
    {
        $subscriptions = $this->getIndividualAllSubscriptionsById($individualId);
        foreach ($subscriptions as $subscription) {
            if ('TYL' == $subscription->Name) {
                $url = "Subscriptions/ChangeProduct/$subscription->Id";

                return $this->callDBS($url, true, null);
            }
        }

        return false;
    }


    //Le return false permet de savoir si on a eu besoin de faire le remove ou non.

    /**
     * @param string $individualId
     * @return bool|string
     * @throws Exception
     */
    public function removeEtylTax(string $individualId): bool|string
    {
        $subscriptions = $this->getIndividualAllSubscriptionsById($individualId);
        foreach ($subscriptions as $subscription) {
            if ('ETYL' == $subscription->Name) {
                $url = "Subscriptions/ChangeProduct/$subscription->Id";

                return $this->callDBS($url, true, null);
            }
        }
        return false;
    }

    /**
     * @param string $individualId
     * @return bool|string
     * @throws Exception
     */
    public function addTylTax(string $individualId): bool|string
    {
        $subscriptions = $this->getIndividualAllSubscriptionsById($individualId);
        foreach ($subscriptions as $subscription) {
            if ('TYL' == $subscription->Name) {
                $url = "Subscriptions/ChangeProduct/$subscription->Id";

                return $this->callDBS($url, true, $this->guids()['subscriptions']['TAX']);
            }
        }

        return false;
    }

    /**
     * @param string $individualId
     * @return false|string
     * @throws Exception
     */
    public function addEtylTax(string $individualId): false|string
    {
        $subscriptions = $this->getIndividualAllSubscriptionsById($individualId);
        foreach ($subscriptions as $subscription) {
            if ('ETYL' == $subscription->Name) {
                $url = "Subscriptions/ChangeProduct/$subscription->Id";

                return $this->callDBS($url, true, $this->guids()['subscriptions']['TAX']);
            }
        }

        return false;
    }

    /**
     * @throws Exception
     */
    public function getIndividualSubscriptionsById(string $individualId)
    {
        $url = "Individual/Subscriptions/{$individualId}";

        return $this->callDBS($url);
    }

    /**
     * @throws Exception
     */
    public function addSubscriptionToIndividual(string $individualId, array $subscriptions)
    {
        $url = "Individual/SubscriptionsAdd/{$individualId}";

        return $this->callDBS($url, true, $subscriptions, true);

    }

    /**
     * @throws Exception
     */
    public function setEndDateToSubscriptions(string $individualId, array $subscriptions)
    {
        $url = "Individual/SubscriptionsEnd/{$individualId}";

        return $this->callDBS($url, true, $subscriptions);

    }

    /**
     * @throws Exception
     */
    public function isSubscriberToFL(string $email): bool
    {
        $subscriptions = $this->getIndividualSubscriptions($email);

        if ($subscriptions) {
            $fl = $this->guids()['subscriptions']['FL'];

            foreach ($subscriptions as $subscription) {
                if ($subscription->Subscription->Id == $fl) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    /**
     * @throws Exception
     */
    public function isSubscriberToNLCdN(string $email): bool
    {
        $subscriptions = $this->getIndividualSubscriptions($email);

        if ($subscriptions) {
            $nl = $this->guids()['subscriptions']['NLCDN'];

            foreach ($subscriptions as $subscription) {
                if ($subscription->Subscription->Id == $nl) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    /**
     * @throws Exception
     */
    public function isSubscriberToPLNL(string $email): bool
    {
        $subscriptions = $this->getIndividualSubscriptions($email);

        if ($subscriptions) {
            $nl =  $this->guids()['subscriptions']['PLNL'];

            foreach ($subscriptions as $subscription) {
                if ($subscription->Subscription->Id == $nl) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }


    /**
     * @param string $individualId
     * @throws Exception
     */
    public function switchSubscriptionEtylToTyl(string $individualId): void
    {
        $this->setEndDateToSubscriptions($individualId, [
            ['Id' => $this->guids()['subscriptions']['ETYL']],
        ]);

        $this->addSubscriptionToIndividual($individualId, [
            ['Id' => $this->guids()['subscriptions']['TYL']],
        ]);
    }

    /**
     * @param string $individualId
     * @throws Exception
     */
    public function switchSubscriptionTylToEtyl(string $individualId): void
    {
        $this->setEndDateToSubscriptions($individualId, [
            ['Id' => $this->guids()['subscriptions']['TYL']],
        ]);

        $this->addSubscriptionToIndividual($individualId, [
            ['Id' => $this->guids()['subscriptions']['ETYL']],
        ]);
    }

    /**
     * @throws Exception
     * @return array-key : 0'hasERF' => bool, 1 hasRF ,2 hasTYL, 3 hasETYL, 4 hasTax, 5 hasENL, 6 hasEINFO,
     *  7 hasNLCDN, 8 hasFL, 9 hasCONT, 10 hasBUL, 11 hasFREEBUL, 12 hasFREEAVI, 13 hasPROMOBUL, 14
     * hasPROMOAVI, 15 hasMM, 16 AVI, 17 EADF
     * @description use php list to easily get the needed information.
     */
    public function getIndividualSubscriptionsBooleanList(string $individualId) : array
    {
        // get individual subscriptions info
        $individualSubscriptions = $this->getIndividualAllSubscriptionsById($individualId);
        $listOfSubscriptions = [];

        foreach ($individualSubscriptions as $subscription) {
            if($subscription->ProductName == 'TAX') {
                $listOfSubscriptions['TAX'] = 'TAX';
            }
            $listOfSubscriptions[$subscription->Subscription->Name] = $subscription->Subscription->Name;
        }
        // todo pass this with enum when all app will run on php 8
        // add new items to the end to not break older code
        return [
            'hasERF' => in_array('ERF', $listOfSubscriptions),
            'hasRF' => in_array('RF', $listOfSubscriptions),
            'hasTYL' => in_array('TYL', $listOfSubscriptions),
            'hasETYL' => in_array('ETYL', $listOfSubscriptions),
            'hasTax' => in_array('TAX', $listOfSubscriptions),
            'hasENL' => in_array('ENL', $listOfSubscriptions),
            'hasEINFO' => in_array('EINFO', $listOfSubscriptions),
            'hasNLCDN' => in_array('NLCDN', $listOfSubscriptions),
            'hasFL' => in_array('FL', $listOfSubscriptions),
            'hasCONT' => in_array('CONT', $listOfSubscriptions),
            'hasBUL' => in_array('BUL', $listOfSubscriptions),
            'hasFREEBUL' => in_array('FREEBUL', $listOfSubscriptions),
            'hasFREEAVI' => in_array('FREEAVI', $listOfSubscriptions),
            'hasPROMOBUL' => in_array('PROMOBUL', $listOfSubscriptions),
            'hasPROMOAVI' => in_array('PROMOAVI', $listOfSubscriptions),
            'hasMM' => in_array('MM', $listOfSubscriptions),
            'hasAVI' => in_array('AVI', $listOfSubscriptions),
            'EADF' => in_array('EADF', $listOfSubscriptions),
        ];

    }

    /**
     * @return array|false
     * @throws Exception
     */
    public function getAllAvailableSubscriptionLists()
    {
        $url = "Subscriptions/Get";
        return $this->callDBS($url);
    }


    /**
     * @param string $subscriptionName
     * @return string
     * @throws Exception
     * @description If zero subscription is found, false will be return.
     */
    public function getSubscriptionGuidFromName(string $subscriptionName): string
    {

        $allSubscriptions = $this->getAllAvailableSubscriptionLists();

        foreach ($allSubscriptions as $subscription) {
            if($subscription->Name == $subscriptionName) {
                return $subscription->Id;
            }
        }
        return false;
    }

}
