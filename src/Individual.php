<?php


namespace PO;

use Exception;

class Individual extends DataBrokerService
{

    private static Individual $_instance;

    public static function getInstance($params): Individual
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new Individual($params);
        }
        return self::$_instance;
    }

    /**
     * @param string $email Give the user email you want to find.
     * @return mixed
     * @throws Exception
     */
    public function getIndividualWithoutDuplication(string $email): mixed
    {
        $email = urlencode($email);
        $url = "Individual/GetAllByEmailAddress/?email={$email}";

        $request = $this->callDBS($url);

        $filtered = [];

        if (empty($request)) {
            return false;
        } else {
            if (1 == count($request)) {
                return $request[0];
            } elseif (count($request) > 1) {
                foreach ($request as $item) {
                    //Mailing = adresse postal
                    if (!$item->InvalidMailingAddress) {
                        $filtered[] = $item;
                    }
                }

                if (empty($filtered)) {
                    // multiple individuals with the same email but postal address not valid
                    // then I take the first one and continue (see with Admin if we want more filter like looking the created date... maybe check here if donation)
                    return $request[0];
                }

                if (1 == count($filtered)) {
                    return $filtered[0];
                }

                $secondFiltered = [];

                if (count($filtered) > 1) {
                    $subscriptionList = ['BUL', 'CONT', 'AVI', 'FREEBUL', 'FREEAVI', 'PROMOBUL', 'MM'];
                    foreach ($filtered as $item) {
                        $url = "IndividualSubscriptions/Get/{$item->Id}";
                        $subscriptions = $this->callDBS($url);

                        // check for empty subscription to avoid error on foreach
                        if(!empty($subscriptions)) {
                            foreach ($subscriptions as $subscription) {
                                if (in_array($subscription->Subscription->Name, $subscriptionList)) {
                                    $secondFiltered[] = $item;
                                    break;
                                }
                            }
                        }
                    }

                    if (empty($secondFiltered)) {
                        return $filtered[0];
                    }

                    if (1 == count($secondFiltered)) {
                        return $secondFiltered[0];
                    }

                    $lastFiltered = [];

                    if (count($secondFiltered) > 1) {
                        foreach ($secondFiltered as $item) {
                            $url = "Individual/Donations/{$item->Id}";
                            $donations = $this->callDBS($url);

                            if ($donations) {
                                $lastFiltered[] = $item;
                            }
                        }

                        if (empty($lastFiltered)) {
                            return $secondFiltered[0];
                        }

                        if (count($lastFiltered) >= 1) {
                            return $lastFiltered[0];
                        } else {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
    }

    /**
     * fields: Email / FirstName / LastName / StartDate / MediaCode / InvalidMailingAddress to
     * true / WelcomeProcessStarted to false / Address1CountryRegion to France by default
     * @param array $fields
     * @return mixed
     * @throws Exception
     */
    public function createIndividual(array $fields): mixed
    {
        $url = 'Individual/Post';

        return $this->callDBS($url, true, $fields);
    }

    /**
     * Get the individual Id by his email.
     * @param string $email
     * @param bool $silent
     * @return false|string|Exception
     * @throws Exception
     */
    public function getIndividualId(string $email, bool $silent = false): false|string|Exception
    {
        $arrayOfIndividuals = $this->getIndividuals($email);

        // Every call with empty body will be return as false.
        if (!$arrayOfIndividuals) {
            //throw new \Exception("No individual with this email address");
            if ($silent) {
                return false;
            } else {
                throw new \Exception("L'adresse e-mail à laquelle vous essayer d'accéder n'existe pas");
            }
        } elseif (is_array($arrayOfIndividuals)) {
            if (count($arrayOfIndividuals) > 1) {
                if ($silent) {
                    return false;
                } else {
                    throw new \Exception("L'adresse e-mail que vous souhaitez modifier est attachée à plusieurs fiches. Veuillez nous contacter à l'adresse contact@portesouvertes.fr");
                }
            }
        } else {
            if ($silent) {
                return false;
            } else {
                throw new \Exception("Nous avons rencontré un problème lors du traitement de votre demande concernant l'e-mail {$email}. Veuillez nous contacter à l'adresse contact@portesouvertes.fr");
                // Abort(404, "Nous avons rencontré un problème lors du traitement de votre demande
                // concernant l'e-mail {$email}. Veuillez nous contacter à l'adresse contact@portesouvertes.fr");
            }
        }

        $individual = $arrayOfIndividuals[0];

        return $individual->Id;
    }

    /**
     * @throws Exception
     */
    public function getIndividuals(string $email): false|array
    {
        $email = urlencode($email);
        $url = "Individual/GetAllByEmailAddress/?email={$email}";

        return ($this->callDBS($url));
    }

    /**
     * @throws Exception
     */
    public function updateIndividual( array $fields): mixed
    {
        $url = 'Individual/Post';

        return $this->callDBS($url, true, $fields);
    }

// connection entité du CRM qui permet de lier une personne à un groupe (antenne église)
    /**
     * @throws Exception
     */
    public function getConnectionsOfTheIndividual(string $individualID)
    {
        $url = "Individual/Connections/{$individualID}";

        return $this->callDBS($url);
    }

    /**
     * @throws Exception
     */
    public function getIndividualGifts(string $individualID)
    {
        $url = "IndividualGift/Get?id={$individualID}";

        return $this->callDBS($url);
    }

}