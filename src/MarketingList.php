<?php

namespace PO;

use Exception;

class MarketingList extends DataBrokerService
{

    private static MarketingList $_instance;

    public static function getInstance($params): MarketingList
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new MarketingList($params);
        }
        return self::$_instance;
    }

    /**
     * @param string $marketingListId
     * @return false|array
     * @throws Exception
     */
    public function getMarketingList(string $marketingListId)
    {
        $url = "MarketingList/Get/{$marketingListId}";

        return $this->callDBS($url);
    }

    /**
     * @param string $marketingListId
     * @return false|array
     * @throws Exception
     */
    public function getMarketingListMembers(string $marketingListId)
    {
        $url = "MarketingList/Members/{$marketingListId}";
        return $this->callDBS($url);
    }

    /**
     * @param int $pageSize
     * @return false|array
     * @throws Exception
     */
    public function getMarketingLists(int $pageSize = 1000)
    {
        $url="MarketingList/GetPaged?page=1&pageSize={$pageSize}";
        return $this->callDBS($url);
    }

}