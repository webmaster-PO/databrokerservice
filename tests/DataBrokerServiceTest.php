<?php


use PO\Activity;
use PO\DataBrokerService;
use PHPUnit\Framework\TestCase;
use PO\Guids;
use PO\Individual;
use PO\Subscription;

class DataBrokerServiceTest extends TestCase
{

    private static DataBrokerService $databrockerServiceClass;
    private static string $endPoint;
    private static string $apiKey;


    public static function setUpBeforeClass(): void
    {
        self::$databrockerServiceClass = new DataBrokerService(['endPoint' => 'https://d365-fr-acc-api.azurewebsites.net/api/',
            'apiKey' => '724A24F2-24B7-4318-A5F9-87ADF5B4CC43']);

        // Individual ACC jouvemichael@gmail.com
        self::$apiKey = '724A24F2-24B7-4318-A5F9-87ADF5B4CC43';
        self::$endPoint = 'https://d365-fr-acc-api.azurewebsites.net/api/';
    }

    public function testSubscription()
    {
        $result = self::$databrockerServiceClass->subscription(['endPoint' => self::$endPoint, 'apiKey' =>
        self::$apiKey]);

        $this->assertInstanceOf(Subscription::class, $result);
    }

    public function testGroup()
    {
        $result = self::$databrockerServiceClass->group(['endPoint' => self::$endPoint, 'apiKey' =>
            self::$apiKey]);

        $this->assertInstanceOf(\PO\Group::class, $result);
    }

    public function testDonation()
    {
        $result = self::$databrockerServiceClass->donation(['endPoint' => self::$endPoint, 'apiKey' =>
            self::$apiKey]);

        $this->assertInstanceOf(\PO\Donation::class, $result);
    }

    public function testGuidsCrm()
    {
        $result = self::$databrockerServiceClass->guidsCrm(['endPoint' => self::$endPoint, 'apiKey' =>
            self::$apiKey]);

        $this->assertEquals("7c1034cf-6fbe-e511-9414-00155d0c180d" , $result['mediaCodes']['IN']);
    }

    public function testGetCountries()
    {
        $result = self::$databrockerServiceClass->getCountries();

        $this->assertIsArray($result);
        $this->assertStringContainsString("France", $result['France']);
    }

    public function testIndividual()
    {
        $result = self::$databrockerServiceClass->individual(['endPoint' => self::$endPoint, 'apiKey' =>
            self::$apiKey]);

        $this->assertInstanceOf(Individual::class, $result);
    }

    public function testActivity()
    {
        $result = self::$databrockerServiceClass->activity(['endPoint' => self::$endPoint, 'apiKey' =>
            self::$apiKey]);

        $this->assertInstanceOf(Activity::class, $result);
    }

    public function testMarketingList()
    {
        $result = self::$databrockerServiceClass->marketingList(['endPoint' => self::$endPoint, 'apiKey' =>
            self::$apiKey]);

        $this->assertInstanceOf(\PO\MarketingList::class, $result);
    }
}
