<?php


use PO\Donation;
use PHPUnit\Framework\TestCase;

class DonationTest extends TestCase
{

    private static Donation $donationClass;
    private static string $individualACCGuid;
    private static string $indivEmail;


    public static function setUpBeforeClass(): void
    {
        self::$donationClass = new Donation(['endPoint' => 'https://d365-fr-acc-api.azurewebsites.net/api/',
            'apiKey' => '724A24F2-24B7-4318-A5F9-87ADF5B4CC43']);

        // Individual ACC jouvemichael@gmail.com
        self::$individualACCGuid = "982ff36a-c33c-ec11-b6e5-000d3ab32f72";
        self::$indivEmail = 'jouvemichael@gmail.com';
    }

    public function testAddDonationToTheBatch()
    {
        $newDonation = [
            "Amount" => 18,
            "Allocations" => [
                [
                    "Amount" => 18,
                    "Destination" => [
                        "Id" => "f49443c3-6fbe-e511-9414-00155d0c180d"
                    ]
                ]
            ],
            "PaymentMethod" => "05141bd6-6fbe-e511-9414-00155d0c180d",
            "PaymentFrequency" => 1,
            "StartDate" => "2022-12-06T08:23:50.386Z",
            "EndDate" => "2022-12-06T08:23:50.386Z",
            "ThankYouLetterSent" => false,
            "IndividualId" => self::$individualACCGuid,
            "MediaCode" => "871034cf-6fbe-e511-9414-00155d0c180d"
        ];

        $result = self::$donationClass->addDonationToTheBatch($newDonation);
        $this->assertIsString($result);
    }

    public function testGetDestinationId()
    {
        $result = self::$donationClass->getDestinationId('74813');
        $this->assertIsString($result);
        $this->assertEquals("069543c3-6fbe-e511-9414-00155d0c180d", $result);

        //with wrong baseReference
        $result = self::$donationClass->getDestinationId('99999');
        $this->assertFalse($result);

    }

    public function testGetInstance()
    {
        $instance = self::$donationClass->getInstance(['endPoint' => 'https://d365-fr-acc-api.azurewebsites.net/api/',
            'apiKey' => '724A24F2-24B7-4318-A5F9-87ADF5B4CC43']);

        $this->assertInstanceOf(Donation::class, $instance );
        $this->assertSame(self::$donationClass, $instance);
    }
}
