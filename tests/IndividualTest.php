<?php declare(strict_types=1);


use PO\Individual;
use PHPUnit\Framework\TestCase;

class IndividualTest extends TestCase
{
    private static Individual $individualClass;
    private static string $individualACCGuid;
    private static string $indivEmail;
    private static string $indivDuplicateEmail;
    private static string $indivDuplicateGoodChoiceId;

    public static function setUpBeforeClass(): void
    {
        self::$individualClass = new Individual(['endPoint' => 'https://d365-fr-acc-api.azurewebsites.net/api/',
            'apiKey' => '724A24F2-24B7-4318-A5F9-87ADF5B4CC43']);

        // Individual ACC jouvemichael@gmail.com
        self::$individualACCGuid = "982ff36a-c33c-ec11-b6e5-000d3ab32f72";
        self::$indivEmail = 'jouvemichael@gmail.com';

        self::$indivDuplicateEmail = 'jouvemichael+duplicate@gmail.com';
        self::$indivDuplicateGoodChoiceId = 'f77cde0b-56df-eb11-bacb-000d3ab3d784';

    }

    public function testCreateIndividual(): void
    {
        $newIndiv =
            [
                'Email' => random_bytes(6) . '@databrocker.fr',
                'FirstName' => 'PrénomTest',
                'LastName' => 'NomTest',
                'MediaCode' => 'f4131bd6-6fbe-e511-9414-00155d0c180d',
                'InvalidMailingAddress' => 'true',
                'WelcomeProcessStarted' => 'false',
                'Address1CountryRegion' => 'France',
                'gender' => 1
            ];

        $result = self::$individualClass->createIndividual($newIndiv);

        $this->assertNotEmpty($result);
    }

    public function testGetIndividualGifts()
    {
        $result = self::$individualClass->getIndividualGifts(self::$individualACCGuid);

        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
    }

    public function testGetConnectionsOfTheIndividual()
    {
        $result = self::$individualClass->getConnectionsOfTheIndividual(self::$individualACCGuid);

        $this->assertIsArray($result);
    }

    public function testUpdateIndividual()
    {
        $randomFirstName = random_bytes(4);
        $result = self::$individualClass->updateIndividual(['Id' => self::$individualACCGuid, 'FirstName' => $randomFirstName]);

        $this->assertEquals($result->FirstName, $randomFirstName);
    }

    public function testGetIndividuals()
    {
        $result = self::$individualClass->getIndividuals(self::$indivEmail);

        $this->assertNotEmpty($result);
    }

    public function testGetIndividualId()
    {
        //Case one individual found
        $result = self::$individualClass->getIndividualId(self::$indivEmail, false);
        $this->assertEquals(self::$individualACCGuid, $result);

        //Case not existe with silent option
        $result = self::$individualClass->getIndividualId('existe@pas.never', true);
        $this->assertSame(false, $result);

        //Case indiv not existe
        $result = self::$individualClass->getIndividualId('existe@pas.never');
        $this->assertContains("L'adresse e-mail à laquelle vous essayer d'accéder n'existe pas", $result['message']);
    }

    public function testGetIndividualIdDuplicateCase()
    {
        $result = self::$individualClass->getIndividualId(self::$indivDuplicateEmail);

        $this->assertStringContainsString("L'adresse e-mail que vous souhaitez modifier est attachée à plusieurs fiches.", $result['message']);
    }

    public function testGetIndividualWithoutDuplication()
    {
        $result = self::$individualClass->getIndividualWithoutDuplication(self::$indivDuplicateEmail);

        $this->assertEquals(self::$indivDuplicateGoodChoiceId, $result->Id);
    }

    public function testGetInstance()
    {
        $instance = self::$individualClass->getInstance(['endPoint' => 'https://d365-fr-acc-api.azurewebsites.net/api/',
            'apiKey' => '724A24F2-24B7-4318-A5F9-87ADF5B4CC43']);

        $this->assertInstanceOf(Individual::class, $instance );
    }

}
