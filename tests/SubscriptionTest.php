<?php


use PO\Subscription;
use PHPUnit\Framework\TestCase;

class SubscriptionTest extends TestCase
{
    private static Subscription $subscriptionClass;
    private static string $individualACCGuid;
    private static string $indivEmail;

    public static function setUpBeforeClass(): void
    {
        self::$subscriptionClass = new Subscription(['endPoint' => 'https://d365-fr-acc-api.azurewebsites.net/api/',
            'apiKey' => '724A24F2-24B7-4318-A5F9-87ADF5B4CC43']);

        // Individual ACC jouvemichael@gmail.com
        self::$individualACCGuid = "982ff36a-c33c-ec11-b6e5-000d3ab32f72";
        self::$indivEmail = 'jouvemichael@gmail.com';

    }

    public function testGetIndividualSubscriptionsById()
    {
        $result = self::$subscriptionClass->getIndividualSubscriptionsById(self::$individualACCGuid);

        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
    }

    public function testSetEndDateToSubscription()
    {

    }

    public function testIsSubscriberToNLCdN()
    {
        $result = self::$subscriptionClass->isSubscriberToNLCdN(self::$indivEmail);
        $result2 = self::$subscriptionClass->isSubscriberToNLCdN('6a2fabd7-7841-eb11-a813-000d3ab9bba9');

        $this->assertEquals(true, $result);
        $this->assertEquals(false, $result2);

    }

    public function testSwitchSubscriptionTylToEtyl()
    {

    }

    public function testHasSubscription()
    {

    }

    public function testGetIndividualAllSubscriptionsById()
    {

    }

    public function testSwitchSubscriptionEtylToTyl()
    {

    }

    public function testIsSubscriberToFL()
    {

    }

    public function testAddEtylTax()
    {

    }

    public function testAddSubscriptionToIndividual()
    {

    }

    public function testRemoveTylTax()
    {

    }

    public function testAddTylTax()
    {

    }

    public function testSetEndDateToSubscriptions()
    {

    }

    public function testIsSubscriberToPLNL()
    {

    }

    public function testGetIndividualSubscriptions()
    {

    }

    public function testGetIndividualSubscriptionsBooleanList()
    {

    }

    public function testRemoveEtylTax()
    {

    }
}
